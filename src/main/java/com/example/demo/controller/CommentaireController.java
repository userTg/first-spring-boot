package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Commentaire;
import com.example.demo.repository.CommentaireRepository;

@RestController
public class CommentaireController {

	@Autowired
	private CommentaireRepository commentaireRepository;

	@RequestMapping(value = "/commentaires", method = RequestMethod.GET)
	public List<Commentaire> getCommentaires() {
		return commentaireRepository.findAll();
	}

	@RequestMapping(value = "/commentaires/{id}", method = RequestMethod.GET)
	public Commentaire getCommentaireById(@PathVariable int id) {
		return commentaireRepository.findById(id).get();
	}

	@RequestMapping(value = "/commentaires", method = RequestMethod.POST)
	public Commentaire save(@RequestBody Commentaire c) {
		return commentaireRepository.save(c);
	}

	@RequestMapping(value = "/commentaires/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Integer id) {
		commentaireRepository.deleteById(id);
		return true;
	}

	@RequestMapping(value = "/commentaires/{id}", method = RequestMethod.PUT)
	public Commentaire save(@PathVariable Integer id, @RequestBody Commentaire c) {
		c.setId(id);
		return commentaireRepository.save(c);
	}

}
