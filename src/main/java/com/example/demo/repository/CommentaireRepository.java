package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Commentaire;

public interface CommentaireRepository extends JpaRepository<Commentaire, Integer> {

}
